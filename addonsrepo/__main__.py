# SPDX-FileCopyrightText: 2024-present Raoul Snyman <raoul@snyman.info>
#
# SPDX-License-Identifier: MIT
from argparse import ArgumentParser

from addonsrepo.app import make_app


parser = ArgumentParser()
parser.add_argument('-b', '--bind', help='The IP addres to bind to')
parser.add_argument('-p', '--port', help='The port to use')
args = parser.parse_args()

app = make_app()
app.run(args.bind, args.port, debug=True)
