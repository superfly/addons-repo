import os
from inspect import getmembers, isfunction, ismethod

from dotenv import load_dotenv


class Settings:
    """Settings"""
    database_url: str = 'sqlite://'
    database_echo: bool = False
    database_check_same_thread: bool = False
    database_expire_on_commit: bool = False
    secret_key: str = ''
    uploads_folder: str = 'uploads'
    base_url: str = 'http://localhost:5000'
    mail_hostname: str = '127.0.0.1'
    mail_port: int = 465
    mail_from: str = ''
    mail_username: str = ''
    mail_password: str = ''
    mail_security: str = 'tls'

    def __init__(self):
        load_dotenv()
        for name, value in getmembers(self):
            if name.startswith('_') or isfunction(value) or ismethod(value):
                continue
            if os.environ.get(name.upper()):
                setattr(self, name, self._smart_cast(os.environ[name.upper()]))
        for key, value in os.environ.items():
            if key.startswith('QUART_'):
                setattr(self, key.lower(), self._smart_cast(value))

    def as_dict(self) -> dict:
        """Return this as a dictionary"""
        config = {}
        for name, value in getmembers(self):
            if name.startswith('_') or isfunction(value) or ismethod(value):
                continue
            config[name.upper()] = getattr(self, name)
        return config

    def _smart_cast(self, value: str) -> int | bool | str:
        """Type cast some values"""
        if value.isdigit():
            return int(value)
        elif value.lower() in ['true', 'false']:
            return value.lower() == 'true'
        return value


settings = Settings()
