# SPDX-FileCopyrightText: 2024-present Raoul Snyman <raoul@snyman.info>
#
# SPDX-License-Identifier: MIT
from quart import Blueprint, abort, render_template
from sqlalchemy.sql.expression import select

from addonsrepo.db import Session
from addonsrepo.models import Page, PageStatus


pages = Blueprint('pages', __name__, url_prefix='/pages')


@pages.route('/<path:slug>', methods=['GET'])
async def view(slug: str):
    with Session() as session:
        page = session.scalars(select(Page).where(Page.slug == slug, Page.status == PageStatus.Published)).first()
        if not page:
            abort(404)
    return await render_template('pages/view.html', page=page)
