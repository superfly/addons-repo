import os
from uuid import UUID

from quart import Blueprint, abort, render_template, request, redirect, url_for, session, flash, send_from_directory
from quart_auth import current_user, login_required
from sqlalchemy.sql.expression import select

from addonsrepo.db import Session
from addonsrepo.models import Addon, AddonStatus, AddonType, AddonVersion, PluginProperties, ThemeProperties, User
from addonsrepo.settings import settings
from addonsrepo.utils import paginate, make_basenames


addons = Blueprint('addons', __name__)


@addons.route('/', methods=['GET'])
@addons.route('/<addon_type>', methods=['GET'])
@addons.route('/<addon_type>/<ordering>', methods=['GET'])
async def index(addon_type: str | None = None, ordering: str | None = None):
    page, offset, limit = await paginate(request)
    with Session() as session:
        query = select(Addon).where(Addon.status == AddonStatus.Approved)
        if addon_type and addon_type.endswith('s'):
            addon_type = addon_type[:-1]
        if addon_type not in [str(item) for item in AddonType]:
            addon_type = None
        if addon_type:
            query = query.where(Addon.addon_type == addon_type)
        if ordering:
            if ordering.lower() == 'rating':
                query = query.order_by(Addon.rating.desc())
                page_title = 'Highest rated'
        else:
            page_title = 'Latest'
            query = query.order_by(Addon.created)
        addons = session.scalars(query.order_by(Addon.created).offset(offset).limit(limit)).all()
    return await render_template('addons/index.html', page_title=page_title, type_=addon_type, addons=addons)


@addons.route('/upload/<addon_type>', methods=['GET'])
@login_required
async def upload(addon_type: str):
    if addon_type not in ['theme', 'plugin']:
        return redirect(url_for('addons.index'))
    return await render_template('addons/upload.html', addon_type=addon_type)


@addons.route('/upload/<addon_type>', methods=['POST'])
@login_required
async def upload_post(addon_type: str):
    if addon_type not in ['theme', 'plugin']:
        return redirect(url_for('addons.index'))
    form = await request.form
    files = await request.files
    if os.path.splitext(files['upload'].filename)[-1] not in ['.otz', '.zip']:
        for key, value in form.items():
            session[key] = value
        await flash(f'Invalid file type for your {addon_type}', 'warning')
        return redirect(url_for('addons.upload', addon_type=addon_type))
    with Session() as sess:
        user = sess.scalars(select(User).where(User.id == current_user.auth_id)).first()
        addon = Addon(user=user, title=form['title'], description=form['description'], addon_type=addon_type,
                      status=AddonStatus.Unreviewed, current_version=AddonVersion(version=form['version']))
        if addon_type == AddonType.Theme:
            addon.theme_properties = ThemeProperties(is_copyright_free=form.get('is_copyright_free') == 'true')
        elif addon_type == AddonType.Plugin:
            addon.plugin_properties = PluginProperties(license=form['license'])
        sess.add(addon)
        sess.commit()
        sess.refresh(addon)
        thumbnail_path, file_path = await make_basenames(addon)
        thumbnail_path += os.path.splitext(files['thumbnail'].filename)[-1]
        file_path += os.path.splitext(files['upload'].filename)[-1]
        addon.thumbnail_path = thumbnail_path
        addon.current_version.file_path = file_path
        addon.current_version.addon_id = addon.id
        sess.commit()
        sess.refresh(addon)
    os.makedirs(f'{settings.uploads_folder}/' + os.path.dirname(thumbnail_path), exist_ok=True)
    os.makedirs(f'{settings.uploads_folder}/' + os.path.dirname(file_path), exist_ok=True)
    await files['thumbnail'].save(f'{settings.uploads_folder}/{thumbnail_path}')
    await files['upload'].save(f'{settings.uploads_folder}/{file_path}')
    await flash(f'Your {addon_type} has been submitted for review', 'success')
    return redirect(url_for('addons.index'))


@addons.route('/addon/<addon_id>', methods=['GET'])
async def view(addon_id: str):
    """View an addon"""
    with Session() as session:
        addon = session.scalars(select(Addon).where(Addon.id == UUID(addon_id),
                                                    Addon.status == AddonStatus.Approved)).first()
        if not addon:
            abort(404)
    return await render_template('addons/view.html', addon=addon)


@addons.route('/thumbnail/<path:path>', methods=['GET'])
async def thumbnail(path: str):
    """View an addon"""
    addon_id, path = path.split('/', 1)
    with Session() as session:
        addon = session.scalars(select(Addon).where(Addon.id == UUID(addon_id))).first()
        if not addon:
            abort(404)
    return await send_from_directory(f'{settings.uploads_folder}/{addon_id}', path)


@addons.route('/download/<path:path>', methods=['GET'])
async def download(path: str):
    """View an addon"""
    addon_id, version, path = path.split('/', 2)
    with Session() as session:
        addon = session.scalars(select(Addon).where(Addon.id == UUID(addon_id))).first()
        if not addon:
            abort(404)
    return await send_from_directory(f'{settings.uploads_folder}/{addon_id}/{version}', path)
