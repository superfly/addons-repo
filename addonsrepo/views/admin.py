# SPDX-FileCopyrightText: 2024-present OpenLP developers
#
# SPDX-License-Identifier: MIT
from zipfile import ZipFile
from uuid import UUID

from quart import Blueprint, abort, flash, render_template, request, redirect, url_for
from quart_auth import login_required
from sqlalchemy.sql.expression import select, func

from addonsrepo.db import Session
from addonsrepo.models import Addon, AddonStatus, AddonType, User, UserStatus, Page, PageStatus
from addonsrepo.settings import settings
from addonsrepo.utils import paginate, admin_required, moderator_required, title2slug


admin = Blueprint('admin', __name__, url_prefix='/admin')


@admin.route('', methods=['GET'])
@login_required
@moderator_required
async def index():
    with Session() as sess:
        total_addons = sess.scalars(select(func.count(Addon.id))).first()
        total_themes = sess.scalars(select(func.count(Addon.id)).where(Addon.addon_type == AddonType.Theme)).first()
        total_plugins = sess.scalars(select(func.count(Addon.id)).where(Addon.addon_type == AddonType.Plugin)).first()
        unreviewed = sess.scalars(select(func.count(Addon.id)).where(Addon.status == AddonStatus.Unreviewed)).first()
    return await render_template('admin/index.html', total_addons=total_addons, total_themes=total_themes,
                                 total_plugins=total_plugins, unreviewed=unreviewed)


@admin.route('/review', methods=['GET'])
@login_required
@moderator_required
async def review():
    page, offset, limit = await paginate(request, per_page=25)
    # Already awaited above, no need now
    addon_type = request.args.get('type', None)
    with Session() as session:
        query = select(Addon).where(Addon.status == AddonStatus.Unreviewed)
        if addon_type and addon_type.endswith('s'):
            addon_type = addon_type[:-1]
        if addon_type not in [str(item) for item in AddonType]:
            addon_type = None
        if addon_type:
            query = query.where(Addon.addon_type == addon_type)
        addons = session.scalars(query.order_by(Addon.created).offset(offset).limit(limit)).all()
        addon_count = session.scalars(select(func.count(Addon.id)).where(query.whereclause)).first()
        page_total = (addon_count // limit) + (1 if addon_count % limit >= 1 else 0)
    return await render_template('admin/review.html', addons=addons, addon_type=addon_type, page=page, offset=offset,
                                 page_total=page_total)


@admin.route('/review/<addon_id>', methods=['GET'])
@login_required
@moderator_required
async def review_addon(addon_id: str):
    """View an addon for approval or denial"""
    with Session() as sess:
        addon = sess.scalars(select(Addon).where(Addon.id == UUID(addon_id))).first()
        if not addon:
            await flash('That addon does not exist', 'danger')
            return redirect(url_for('admin.review'))
        with ZipFile(f'{settings.uploads_folder}/{addon.current_version.file_path}', 'r') as zipf:
            file_list = zipf.namelist()
        thumbnail_path = addon.thumbnail_path.replace(f'{addon.id}/', '')
    return await render_template('admin/review_addon.html', addon=addon, thumbnail_path=thumbnail_path,
                                 file_list=file_list)


@admin.route('/review/<addon_id>', methods=['POST'])
@login_required
@moderator_required
async def review_addon_post(addon_id: str):
    """Approve or deny an addon"""
    with Session() as sess:
        addon = sess.scalars(select(Addon).where(Addon.id == UUID(addon_id))).first()
        if not addon:
            await flash('That addon does not exist', 'danger')
            return redirect(url_for('admin.review'))
        form = await request.form
        if form.get('action') == 'approved':
            addon.status = AddonStatus.Approved
        elif form.get('action') == 'denied':
            addon.status = AddonStatus.Denied
        sess.add(addon)
        sess.commit()
    await flash(f'Addon successfully {form["action"]}', 'success')
    return redirect(url_for('admin.review'))


@admin.route('/users', methods=['GET'])
@login_required
@admin_required
async def users():
    page, offset, limit = await paginate(request, per_page=25)
    with Session() as session:
        users = session.scalars(select(User).order_by(User.username).offset(offset).limit(limit)).all()
        user_count = session.scalars(select(func.count(User.id))).first()
        page_total = (user_count // limit) + (1 if user_count % limit >= 1 else 0)
    return await render_template('admin/users.html', users=users, page=page, offset=offset, page_total=page_total)


@admin.route('/users/edit/<int:user_id>', methods=['GET'])
@login_required
@admin_required
async def edit_user(user_id: int):
    """Edit an existing user"""
    with Session() as session:
        user = session.scalars(select(User).where(User.id == user_id)).first()
    return await render_template('admin/edit_user.html', user=user, user_statuses=UserStatus)


@admin.route('/users/save/<int:user_id>', methods=['POST'])
@login_required
@admin_required
async def save_user(user_id: int | None = None):
    """Save the user"""
    form = await request.form
    with Session() as session:
        if user_id:
            user = session.scalars(select(User).where(User.id == user_id)).first()
            if not user:
                abort(404)
            user.username = form['username']
            user.email = form['email']
            # if form['password']:
            #     user.password = bcrypt.generate_password_hash(form['password'])
            user.status = form['status']
            user.is_admin = form.get('is_admin', 'false') == 'true'
            user.is_moderator = form.get('is_moderator', 'false') == 'true'
        else:
            user = Page(title=form['title'], slug=form['slug'] or title2slug(form['title']), content=form['content'],
                        status=form['status'])
        session.add(user)
        session.commit()
    await flash('Page saved', 'success')
    return redirect(url_for('admin.users'))


@admin.route('/pages', methods=['GET'])
@login_required
@admin_required
async def pages():
    page, offset, limit = await paginate(request, per_page=25)
    with Session() as session:
        pages = session.scalars(select(Page).order_by(Page.slug).offset(offset).limit(limit)).all()
        page_count = session.scalars(select(func.count(Page.id))).first()
        page_total = (page_count // limit) + (1 if page_count % limit >= 1 else 0)
    return await render_template('admin/pages.html', pages=pages, page=page, offset=offset, page_total=page_total)


@admin.route('/pages/new', methods=['GET'])
@login_required
@admin_required
async def new_page():
    """Add a new page"""
    return await render_template('admin/edit_page.html', page=None, page_statuses=PageStatus)


@admin.route('/pages/edit/<int:page_id>', methods=['GET'])
@login_required
@admin_required
async def edit_page(page_id: int):
    """Edit an existing page"""
    with Session() as session:
        page = session.scalars(select(Page).where(Page.id == page_id)).first()
    return await render_template('admin/edit_page.html', page=page, page_statuses=PageStatus)


@admin.route('/pages/save', methods=['POST'])
@admin.route('/pages/save/<int:page_id>', methods=['POST'])
@login_required
@admin_required
async def save_page(page_id: int | None = None):
    """Save the page"""
    form = await request.form
    with Session() as session:
        if page_id:
            page = session.scalars(select(Page).where(Page.id == page_id)).first()
            if not page:
                abort(404)
            page.title = form['title']
            page.slug = form['slug'] or title2slug(form['title'])
            page.content = form['content']
            page.status = form['status']
        else:
            page = Page(title=form['title'], slug=form['slug'] or title2slug(form['title']), content=form['content'],
                        status=form['status'])
        session.add(page)
        session.commit()
    await flash('Page saved', 'success')
    return redirect(url_for('admin.pages'))
