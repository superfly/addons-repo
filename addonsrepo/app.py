# SPDX-FileCopyrightText: 2024-present Raoul Snyman <raoul@snyman.info>
#
# SPDX-License-Identifier: MIT
from markdown_it import MarkdownIt
from quart import Quart, Response, flash, redirect, url_for
from quart_auth import QuartAuth, Unauthorized, current_user
from sqlalchemy.sql.expression import select

from addonsrepo.bcrypt import bcrypt
from addonsrepo.db import db, Session
from addonsrepo.models import User
from addonsrepo.settings import settings
from addonsrepo.views.addons import addons
from addonsrepo.views.admin import admin
from addonsrepo.views.pages import pages
from addonsrepo.views.user import user


def make_app() -> Quart:
    """Create an application instance"""
    app = Quart('addonsrepo')
    app.secret_key = settings.secret_key
    app.config.update(settings.as_dict())
    bcrypt.init_app(app)
    db.init_app(app)
    db.create_all()
    QuartAuth(app)
    app.register_blueprint(admin)
    app.register_blueprint(pages)
    app.register_blueprint(user)
    # This one always needs to be last
    app.register_blueprint(addons)

    @app.errorhandler(Unauthorized)
    async def redirect_to_login(*_: Exception) -> Response:
        """Catch the Unauthorized exception and redirect the user to the login page"""
        await flash('Please log in to continue', 'warning')
        return redirect(url_for('user.login'))

    @app.context_processor
    async def extra_contexts():
        """Add some contexts to the templates"""
        the_user: User | None = None
        is_authenticated = await current_user.is_authenticated
        if is_authenticated:
            with Session() as session:
                the_user = session.scalars(select(User).where(User.id == current_user.auth_id)).first()
        return {'logged_in_user': the_user}

    @app.template_filter('markdown')
    async def render_markdown(text: str) -> str:
        """Render markdown to HTML via a filter"""
        md = MarkdownIt().enable('table')
        return md.render(text)

    return app
