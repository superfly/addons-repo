document.addEventListener("DOMContentLoaded", function() {
  document.getElementById("update-slug").addEventListener("click", function () {
    document.getElementById("slug").value = document.getElementById("title").value.toLowerCase().replace(/[^a-zA-Z0-9]+/g,'-');
  });
});
