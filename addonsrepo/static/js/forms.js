document.addEventListener("DOMContentLoaded", function() {
  console.log('Setting up form validation');
  Array.from(document.forms).forEach(function(form) {
    console.log('Form validation');
    form.addEventListener('submit', function(event) {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')
    }, false);
  })
});
